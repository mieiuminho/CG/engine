# Engine

## Usage

This program is meant to draw the scenes that it gets as XML files
as arguments.

To use the program simply run:

`./engine <scene_file>.XML`

## Engine Controls

`r` -> toggle rotation

`d` -> rotate to the right

`e` -> rotate to the left

`l` -> show the object only with lines

`s` -> show the object in solid mode

## Development

Compile and run the program.

```
bin/run config.xml
```

Build the target. You can build in debug mode by adding the flag `-g`.

```
bin/build [-g]
```

Open the binary in the debugger.

```
bin/debug
```

Format the code.

```
bin/format
```

Clean the project directory.

```
bin/clean
```

