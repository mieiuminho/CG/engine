#include "decoder.h"

vector<float> parseVertex(const char *line) {

    vector<float> r;

    std::vector<std::string> results;
    std::string token;
    std::istringstream tokenStream(line);

    // Splits the line by a given character, given as a parameter in the
    // getline inside the while condition. In this case the character is a
    // comma.
    while (std::getline(tokenStream, token, ',')) {
        results.push_back(token);
    }

    // Converts the strings to float and puts them in the array
    for (int i = 0; i < results.size(); i++) {
        r.push_back(stof(results.at(i)));
    }

    return r;
}

/** Given a .3d file path, opens, reads and parses the information into a
 * matrix of floats. Each entry of the outer array(row) has size 3,
 * representing a vertex. N rows represent N vertices.
 * @param path : path of the file. Must be a .3d in order to work.  */

Model decode(const char *filePath) {

    /*Declarations line will be used as a buffer for the lines read from the
     * file.  myfile is an input file stream, it attempts to open the file at
     * the path passed as parameter r will be returned at the end of the
     * function. It is initialized as a null pointer.  */
    std::string line;

    std::ifstream myfile(filePath);

    vector<float> points;
    vector<float> normals;
    vector<float> textCoords;
    vector<unsigned int> indexes;

    try {
        /*
         * Checks if the file is open. If it is not, something went wrong and
         * the function will not continue it's normal execution.  */

        if (myfile.is_open()) {

            getline(myfile, line);

            getline(myfile, line);

            int num_points = atoi(line.c_str());

            // For each subsequent lines, reads it and calls an auxiliary
            // function to parse it, convert it into a vertex (array of floats)
            // and insert it into the corresponding row of the outer array.
            for (int i = 0; i < num_points; i++) {

                getline(myfile, line);
                vector<float> vertices = parseVertex(line.c_str());

                for (int i = 0; i < vertices.size(); i++) {
                    points.push_back(vertices.at(i));
                }
            }

            getline(myfile, line);

            getline(myfile, line);

            int num_indexes = atoi(line.c_str());

            for (int i = 0; i < num_indexes; i++) {

                getline(myfile, line);

                indexes.push_back((unsigned int)atoi(line.c_str()));
            }
            getline(myfile, line);

            getline(myfile, line);

            int num_normals = atoi(line.c_str());

            for (int i = 0; i < num_normals; i++) {

                getline(myfile, line);
                vector<float> vertices = parseVertex(line.c_str());

                for (int i = 0; i < vertices.size(); i++) {
                    normals.push_back(vertices.at(i));
                }
            }

            getline(myfile, line);

            getline(myfile, line);

            int num_textCoords = atoi(line.c_str());

            for (int i = 0; i < num_textCoords; i++) {

                getline(myfile, line);
                vector<float> vertices = parseVertex(line.c_str());

                for (int i = 0; i < vertices.size(); i++) {
                    textCoords.push_back(vertices.at(i));
                }
            }

            return Model(points, normals, textCoords, indexes);

        } else {

            cerr << "Error: " << strerror(errno) << "\n";
        }
    } catch (const std::exception e) {

        std::cout << "Error: Could not read .3d file\n";
    }
}
