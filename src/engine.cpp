#ifdef __APPLE__
#include <GLUT/glut.h>
#else

#include "../lib/glew.h"
#include "utils/Camera.h"
#include "utils/Lighter.h"
#include "utils/Renderer.h"
#include "utils/Scene.h"
#include "utils/Transformer.h"
#include "utils/VBOUtils.h"
#include "xmlParser.h"
#include <GL/glut.h>
#include <iostream>

#endif

using namespace std;
using namespace tinyxml2;

// Camera Object
Camera *Camera ::c = nullptr;
Camera *camera = Camera::getInstance();

// Trasnformer Object
Transformer t = Transformer();

// Ligher Object
Lighter l = Lighter();

// VBOUtils Object
VBOUtils vbos = VBOUtils();

// Renderer Object
Renderer r = Renderer();

// Scene Object
Scene s = Scene();

void renderScene(void) {

    // clear buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // set the camera
    glLoadIdentity();
    gluLookAt(camera->getPos().getX(), camera->getPos().getY(),
              camera->getPos().getZ(), camera->getLookAt().getX(),
              camera->getLookAt().getY(), camera->getLookAt().getZ(), 0.0f,
              1.0f, 0.0f);

    for (int i = 0; i < s.getActions().size(); i++) {
        char *act = (char *)malloc(sizeof(char) * 3);
        act[0] = s.getActions().at(i)[0];
        act[1] = s.getActions().at(i)[1];
        act[2] = '\0';

        switch (getActionVal(act)) {
            case PU: {
                glPushMatrix();
                break;
            }
            case PO: {
                glPopMatrix();
                break;
            }
            case DT: {
                t.translateDynamic(s.getActions().at(i), r.getT0());
                break;
            }
            case ST: {
                t.translateStatic(s.getActions().at(i));
                break;
            }
            case DR: {
                t.rotateDynamic(s.getActions().at(i), r.getT0());
                break;
            }
            case SR: {
                t.rotateStatic(s.getActions().at(i));
                break;
            }
            case S: {
                t.scale(s.getActions().at(i));
                break;
            }
            case MT: {
                r.drawTextured(vbos, s.getActions().at(i), s.getTextures(),
                               s.getIndexes());
                break;
            }
            case MM: {
                r.drawMaterial(vbos, s.getActions().at(i), s.getIndexes());
                break;
            }
            case LP: {
                l.lightPD(s.getActions().at(i), 1, r.isFirstFrame());
                break;
            }
            case LD: {
                l.lightPD(s.getActions().at(i), 0, r.isFirstFrame());
                break;
            }
            case LS: {
                l.lightS(s.getActions().at(i), r.isFirstFrame());
                break;
            }
        }
    }

    // End of frame
    glutSwapBuffers();
    l.setLightCounter(0);
    if (r.isFirstFrame())
        r.setFirstFrame(false);
}

int main(int argc, char **argv) {

    // The program must be called with exactly 1 argument. The path of the
    // config file
    if (argc != 2) {
        cout << "Invalid number of arguments\n";
        return -1;
    }

    // Initializes the models vector

    glutInit(&argc, argv);
    r.setT0(glutGet(GLUT_ELAPSED_TIME) / 1000.0);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(1600, 900);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Engine");

    // put callback registration here
    glutDisplayFunc(renderScene);
    glutReshapeFunc(Renderer ::changeSize);
    glutKeyboardFunc(Camera ::keyFunc);
    glutMouseFunc(Camera ::processMouseButtons);
    glutMotionFunc(Camera ::processMouseMotion);
    glutIdleFunc(Renderer ::idle);

#ifndef __APPLE__
    glewInit();
#endif

    vbos.allocate(s);

    // OpenGL settings
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_NORMALIZE);
    glEnable(GL_BLEND);

    s.buildScene(argv[1]);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    vbos.generateBuffers(s);

    r.bindToBuffers(vbos, s);

    // enter GLUT's main loop
    glutMainLoop();

    return 1;
}
