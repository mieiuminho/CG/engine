//
// Created by hugo on 08/04/20.
//

#include "splineCalculator.h"
#include <iostream>

double *scalarMul(double *mat, int size, double scalar) {

    double *r = (double *)malloc(sizeof(double) * size);

    for (int i = 0; i < size; i++) {
        r[i] = mat[i] * scalar;
    }

    return r;
}

double *matSum(double *mat1, double *mat2, int size) {
    double *r = (double *)malloc(sizeof(double) * size);

    for (int i = 0; i < size; i++) {
        r[i] = mat1[i] + mat2[i];
    }

    return r;
}

double *matSub(double *mat1, double *mat2, int size) {
    double *r = (double *)malloc(sizeof(double) * size);

    for (int i = 0; i < size; i++) {
        r[i] = mat1[i] - mat2[i];
    }

    return r;
}

double *mul3DVect(double *v1, double *v2) {

    double *r = (double *)malloc(sizeof(double) * 3);
    r[0] = v1[1] * v2[2] - v1[2] * v2[1];
    r[1] = v1[2] * v2[0] - v1[0] * v2[2];
    r[2] = v1[0] * v2[1] - v1[1] * v2[0];
    return r;
}

void normalize(double *v, int size) {
    double a = 0;
    for (int i = 0; i < size; i++) {
        a += pow(v[i], 2);
    }
    a = sqrt(a);
    if (a != 0) {
        for (int i = 0; i < size; i++) {
            v[i] /= a;
        }
    }
}

void copyVector(double *src, double *dest, int size) {
    for (int i = 0; i < size; i++)
        dest[i] = src[i];
}

double *calculateX(vector<double *> controlPoints, int seg, double t) {

    double *prev = controlPoints.at(seg - 1);
    double *currentStart = controlPoints.at(seg);
    double *currentEnd = controlPoints.at(seg + 1);
    double *next = controlPoints.at(seg + 2);

    double tSquared = t * t;

    double *one = scalarMul(prev, 3, (-0.5 + 2 * t - (1.5) * tSquared));
    double *two = scalarMul(currentStart, 3, (-5 * t + (4.5) * tSquared));
    double *three = scalarMul(currentEnd, 3, (0.5 + 4 * t - (4.5) * tSquared));
    double *four = scalarMul(next, 3, (-t + (1.5) * tSquared));

    double *firstSum = matSum(one, two, 3);
    double *secondSum = matSum(firstSum, three, 3);
    double *X = matSum(secondSum, four, 3);

    free(one);
    free(two);
    free(three);
    free(four);
    free(firstSum);
    free(secondSum);

    return X;
}

void calculateXYZ(double *X, double *Y, double *Z,
                  vector<double *> controlPoints, int seg, double t) {

    double Yprev[3] = {0, 1, 0};
    double *XBuf = calculateX(controlPoints, seg, t);
    double *ZBuf = mul3DVect(XBuf, Yprev);
    double *YBuf = mul3DVect(XBuf, ZBuf);
    copyVector(XBuf, X, 3);
    copyVector(YBuf, Y, 3);
    copyVector(ZBuf, Z, 3);

    normalize(X, 3);
    normalize(Y, 3);
    normalize(Z, 3);

    // free(Xprev);
    // free(Yprev);
    // free(Zprev);
}

double *getRotationMatrix(vector<double *> controlPoints, int seg, double t) {
    double *r = (double *)malloc(sizeof(double) * 16);
    double *XI = (double *)malloc(sizeof(double) * 3);
    double *YI = (double *)malloc(sizeof(double) * 3);
    double *ZI = (double *)malloc(sizeof(double) * 3);
    calculateXYZ(XI, YI, ZI, controlPoints, seg, t);
    for (int i = 0; i < 3; i++) {
        r[i] = XI[i];
    }
    r[3] = 0;
    for (int i = 4; i < 7; i++) {
        r[i] = YI[i - 4];
    }
    r[7] = 0;
    for (int i = 8; i < 11; i++) {
        r[i] = ZI[i - 8];
    }
    r[11] = 0;
    for (int i = 12; i < 15; i++) {
        r[i] = 0;
    }
    r[15] = 1;

    return r;
}

double *calculatePoint(vector<double *> controlPoints, double *rotMat,
                       double totalTime, double instant) {
    double *point;

    if (instant >= totalTime) {
        int complete_translations = instant / totalTime;
        instant -= totalTime * complete_translations;
    }

    int num_segments = controlPoints.size() - 3;

    double time_step = totalTime / num_segments;
    int segment = (int)(instant / time_step + 1);

    double *prev = controlPoints.at(segment - 1);
    double *currentStart = controlPoints.at(segment);
    double *currentEnd = controlPoints.at(segment + 1);
    double *next = controlPoints.at(segment + 2);

    double t = (instant - ((segment - 1) * time_step)) / time_step;

    double tSquared = t * t;
    double tCubed = tSquared * t;

    double *one = scalarMul(prev, 3, (-0.5 * tCubed + tSquared - 0.5 * t));
    double *two = scalarMul(currentStart, 3,
                            (1 + 0.5 * tSquared * (-5) + 0.5 * tCubed * 3));
    double *three = scalarMul(currentEnd, 3,
                              (0.5 * tCubed * (-3) + 0.5 * t + 2 * tSquared));
    double *four = scalarMul(next, 3, (-0.5 * tSquared + 0.5 * tCubed));

    double *firstSum = matSum(one, two, 3);
    double *secondSum = matSum(firstSum, three, 3);
    point = matSum(secondSum, four, 3);

    free(one);
    free(two);
    free(three);
    free(four);
    free(firstSum);
    free(secondSum);

    /*
    (-.5f * tension * tCubed + tension * tSquared - .5f * tension * t) * prev +
    (1 + .5f * tSquared * (tension - 6) + .5f * tCubed * (4 - tension)) *
    currStart +
    (.5f * tCubed * (tension - 4) + .5f * tension * t - (tension - 3) *
    tSquared) * currEnd +
    (-.5f * tension * tSquared + .5f * tension * tCubed) * next;
    */

    double *buf = getRotationMatrix(controlPoints, segment, t);

    for (int i = 0; i < 16; i++)
        rotMat[i] = buf[i];

    return point;
}
