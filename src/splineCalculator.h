//
// Created by hugo on 08/04/20.
//

#ifndef ENGINE_SPLINECALCULATOR_H
#define ENGINE_SPLINECALCULATOR_H

#include <math.h>
#include <stdlib.h>
#include <vector>

using namespace std;

double *calculatePoint(vector<double *> controlPoints, double *rotMat,
                       double totalTime, double instant);

#endif // ENGINE_SPLINECALCULATOR_H
