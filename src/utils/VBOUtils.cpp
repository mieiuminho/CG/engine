#include "VBOUtils.h"

GLuint *VBOUtils ::getBuffers() { return this->buffers; }

GLuint *VBOUtils ::getIndexes() { return this->indexes; }

GLuint *VBOUtils ::getNormals() { return this->normals; }

GLuint *VBOUtils ::getTextureCoordinates() { return this->textureCoordinates; }

void VBOUtils ::allocate(Scene s) {

    this->indexes = (GLuint *)malloc(sizeof(GLuint) * s.getIndexes().size());
    this->buffers =
        (GLuint *)malloc(sizeof(GLuint) * s.getModelPoints().size());
    this->normals = (GLuint *)malloc(sizeof(GLuint) * s.getNormals().size());
    this->textureCoordinates =
        (GLuint *)malloc(sizeof(GLuint) * s.getTextureCoordinates().size());
}

void VBOUtils ::generateBuffers(Scene s) {

    glGenBuffers(s.getIndexes().size(), this->indexes);
    glGenBuffers(s.getModelPoints().size(), this->buffers);
    glGenBuffers(s.getNormals().size(), this->normals);
    glGenBuffers(s.getTextureCoordinates().size(), this->textureCoordinates);
}