#include "Renderer.h"

double Renderer ::getT0() { return this->t0; }

bool Renderer ::isFirstFrame() { return this->firstFrame; }

void Renderer ::setT0(double t0) { this->t0 = t0; }

void Renderer ::setFirstFrame(bool firstFrame) {

    this->firstFrame = firstFrame;
}

void Renderer ::drawTextured(VBOUtils vbos, char *act,
                             vector<unsigned int> textures,
                             vector<vector<unsigned int>> __indexes__) {

    char *buff = strdup(act);
    char *token = strtok(buff, " ");
    token = strtok(NULL, " ");
    if (!token) {
        cout << "ERRO: Could not parse model\n";
    }

    int pos = atoi(token);
    token = strtok(NULL, " ");
    int posText = atoi(token);
    token = strtok(NULL, " ");

    if (strcmp(token, "true") == 0) {
        token = strtok(NULL, " ");
        float diff[4];
        diff[3] = 1.0;
        diff[0] = atof(token);
        token = strtok(NULL, " ");
        diff[1] = atof(token);
        token = strtok(NULL, " ");
        diff[2] = atof(token);

        // Parse the spec component
        token = strtok(NULL, " ");
        float spec[4];
        spec[3] = 1.0;
        spec[0] = atof(token);
        token = strtok(NULL, " ");
        spec[1] = atof(token);
        token = strtok(NULL, " ");
        spec[2] = atof(token);

        // Parse the emissive component
        token = strtok(NULL, " ");
        float emiss[4];
        emiss[3] = 1.0;
        emiss[0] = atof(token);
        token = strtok(NULL, " ");
        emiss[1] = atof(token);
        token = strtok(NULL, " ");
        emiss[2] = atof(token);

        // Parse the ambient component
        token = strtok(NULL, " ");
        float amb[4];
        amb[3] = 1.0;
        amb[0] = atof(token);
        token = strtok(NULL, " ");
        amb[1] = atof(token);
        token = strtok(NULL, " ");
        amb[2] = atof(token);

        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);

        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);

        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emiss);

        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);

        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 128.0f);

    } else {

        float dark[4] = {0, 0, 0, 1};
        float white[4] = {1.0f, 1.0f, 1.0f, 1.0f};

        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, dark);

        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, white);

        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);

        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, dark);

        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 128.0f);
    }

    glBindTexture(GL_TEXTURE_2D, textures.at(posText));

    glBindBuffer(GL_ARRAY_BUFFER, vbos.getBuffers()[pos]);
    glVertexPointer(3, GL_FLOAT, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, vbos.getNormals()[pos]);
    glNormalPointer(GL_FLOAT, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, vbos.getTextureCoordinates()[pos]);
    glTexCoordPointer(2, GL_FLOAT, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos.getIndexes()[pos]);
    glDrawElements(GL_TRIANGLES, __indexes__.at(pos).size(), GL_UNSIGNED_INT,
                   nullptr);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer ::drawMaterial(VBOUtils vbos, char *act,
                             vector<vector<unsigned int>> __indexes__) {
    char *buff = strdup(act);
    char *token = strtok(buff, " ");
    token = strtok(NULL, " ");

    if (!token) {
        cout << "ERRO: Could not parse model\n";
    }

    int pos = atoi(token);
    // Parse the diffuse component
    token = strtok(NULL, " ");
    float diff[4];
    diff[3] = 1.0;
    diff[0] = atof(token);
    token = strtok(NULL, " ");
    diff[1] = atof(token);
    token = strtok(NULL, " ");
    diff[2] = atof(token);

    // Parse the spec component
    token = strtok(NULL, " ");
    float spec[4];
    spec[3] = 1.0;
    spec[0] = atof(token);
    token = strtok(NULL, " ");
    spec[1] = atof(token);
    token = strtok(NULL, " ");
    spec[2] = atof(token);

    // Parse the emissive component
    token = strtok(NULL, " ");
    float emiss[4];
    emiss[3] = 1.0;
    emiss[0] = atof(token);
    token = strtok(NULL, " ");
    emiss[1] = atof(token);
    token = strtok(NULL, " ");
    emiss[2] = atof(token);

    // Parse the ambient component
    token = strtok(NULL, " ");
    float amb[4];
    amb[3] = 1.0;
    amb[0] = atof(token);
    token = strtok(NULL, " ");
    amb[1] = atof(token);
    token = strtok(NULL, " ");
    amb[2] = atof(token);

    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diff);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emiss);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, amb);

    glBindBuffer(GL_ARRAY_BUFFER, vbos.getBuffers()[pos]);
    glVertexPointer(3, GL_FLOAT, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, vbos.getNormals()[pos]);
    glNormalPointer(GL_FLOAT, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos.getIndexes()[pos]);
    glDrawElements(GL_TRIANGLES, __indexes__.at(pos).size(), GL_UNSIGNED_INT,
                   nullptr);
}

void Renderer ::bindToBuffers(VBOUtils vbos, Scene s) {

    for (int i = 0; i < s.getModelPoints().size(); i++) {

        glBindBuffer(GL_ARRAY_BUFFER, vbos.getBuffers()[i]);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * s.getModelPoints().at(i).size(),
                     s.getModelPoints().at(i).data(), GL_STATIC_DRAW);
    }

    for (int i = 0; i < s.getIndexes().size(); i++) {

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbos.getIndexes()[i]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     sizeof(unsigned int) * s.getIndexes().at(i).size(),
                     s.getIndexes().at(i).data(), GL_STATIC_DRAW);
    }

    for (int i = 0; i < s.getNormals().size(); i++) {
        glBindBuffer(GL_ARRAY_BUFFER, vbos.getNormals()[i]);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * s.getNormals().at(i).size(),
                     s.getNormals().at(i).data(), GL_STATIC_DRAW);
    }

    for (int i = 0; i < s.getTextureCoordinates().size(); i++) {
        glBindBuffer(GL_ARRAY_BUFFER, vbos.getTextureCoordinates()[i]);
        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(float) * s.getTextureCoordinates().at(i).size(),
                     s.getTextureCoordinates().at(i).data(), GL_STATIC_DRAW);
    }
}
