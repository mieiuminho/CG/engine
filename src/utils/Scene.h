#ifndef ENGINE_SCENE_H
#define ENGINE_SCENE_H
#include "../decoder.h"
#include "../xmlParser.h"
#include <IL/il.h>
#include <vector>

using namespace std;

class Scene {

  private:
    vector<vector<float>> modelPoints;
    vector<vector<float>> normals;
    vector<vector<float>> textureCoordinates;
    vector<vector<unsigned int>> indexes;
    vector<const char *> modelFiles;
    vector<const char *> textureFiles;
    vector<unsigned int> textures;
    vector<char *> actions;

  public:
    Scene() = default;

    vector<vector<float>> getModelPoints();

    vector<vector<float>> getNormals();

    vector<vector<float>> getTextureCoordinates();

    vector<vector<unsigned int>> getIndexes();

    vector<const char *> getModelFiles();

    vector<const char *> getTextureFiles();

    vector<unsigned int> getTextures();

    vector<char *> getActions();

    void setModelPoints(vector<vector<float>> modelPoints);

    void setNormals(vector<vector<float>> normals);

    void setTextureCoordinates(vector<vector<float>> textureCoordinates);

    void setIndexes(vector<vector<unsigned int>> indexes);

    void setModelFiles(vector<const char *> modelFiles);

    void setTextureFiles(vector<const char *> textureFiles);

    void setTextures(vector<unsigned int> textures);

    void setActions(vector<char *> actions);

    void buildScene(char *configPath);

    void initTextures();
};

#endif
