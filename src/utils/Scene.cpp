#include "Scene.h"

vector<vector<float>> Scene ::getModelPoints() { return this->modelPoints; }

vector<vector<float>> Scene ::getNormals() { return this->normals; }

vector<vector<float>> Scene ::getTextureCoordinates() {

    return this->textureCoordinates;
}

vector<vector<unsigned int>> Scene ::getIndexes() { return this->indexes; }

vector<const char *> Scene ::getModelFiles() { return this->modelFiles; }

vector<const char *> Scene ::getTextureFiles() { return this->textureFiles; }

vector<unsigned int> Scene ::getTextures() { return this->textures; }

vector<char *> Scene ::getActions() { return this->actions; }

void Scene ::setModelPoints(vector<vector<float>> modelPoints) {

    this->modelPoints = modelPoints;
}

void Scene ::setNormals(vector<vector<float>> normals) {

    this->normals = normals;
}

void Scene ::setTextureCoordinates(vector<vector<float>> textureCoordinates) {

    this->textureCoordinates = textureCoordinates;
}

void Scene ::setIndexes(vector<vector<unsigned int>> indexes) {

    this->indexes = indexes;
}

void Scene ::setModelFiles(vector<const char *> modelFiles) {

    this->modelFiles = modelFiles;
}

void Scene ::setTextureFiles(vector<const char *> textureFiles) {

    this->textureFiles = textureFiles;
}

void Scene ::setTextures(vector<unsigned int> textures) {

    this->textures = textures;
}

void Scene ::setActions(vector<char *> actions) { this->actions = actions; }

void Scene ::buildScene(char *configPath) {

    this->actions.clear();
    this->modelFiles.clear();
    this->modelPoints.clear();
    this->textureFiles.clear();
    this->normals.clear();
    this->textures.clear();

    this->actions =
        parse(configPath, &(this->modelFiles), &(this->textureFiles));

    for (int i = 0; i < this->actions.size(); i++) {
        cout << this->actions.at(i) << "\n";
    }

    for (int i = 0; i < this->modelFiles.size(); i++) {

        Model m = decode(this->modelFiles.at(i));
        this->modelPoints.push_back(m.getPoints());
        this->indexes.push_back(m.getIndexes());
        this->normals.push_back(m.getNormals());
        this->textureCoordinates.push_back(m.getTextCoords());
    }

    this->initTextures();
}

void Scene ::initTextures() {

    ilInit();

    for (int i = 0; i < this->textureFiles.size(); i++) {

        unsigned int buf;
        unsigned int t, th, tw;
        unsigned char *textData;
        ilGenImages(1, &t);
        ilBindImage(t);
        bool test = ilLoadImage((ILstring)this->textureFiles.at(i));

        if (!test) {
            cout << "ERROR: Could not read texture file: "
                 << this->textureFiles.at(i) << "\n";
            return;
        }

        tw = ilGetInteger(IL_IMAGE_WIDTH);
        th = ilGetInteger(IL_IMAGE_HEIGHT);
        ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE);
        textData = ilGetData();
        glGenTextures(1, &buf);
        this->textures.push_back(buf);
        glBindTexture(GL_TEXTURE_2D, this->textures.at(i));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tw, th, 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, textData);

        glGenerateMipmap(GL_TEXTURE_2D);
    }
}
