#include "Lighter.h"

using namespace std;

void Lighter ::setLightCounter(int lightCounter) {

    this->lightCounter = lightCounter;
}

void Lighter ::lightS(char *act, int firstFrame) {

    char *buf = strdup(act);
    char *token = strtok(buf, " ");
    int lightConstant = getLightConstant(this->lightCounter++);

    if (lightConstant == ERROR && firstFrame) {
        cout << "ERROR: Maximum Number of Lights Exceeded(8). light  #"
             << lightCounter << " will be ignored"
             << "\n";
        return;
    }
    glEnable(lightConstant);
    float pos[4];
    float dir[3];
    float cutOffAngle = 0;

    token = strtok(NULL, " ");
    cutOffAngle = atof(token);

    for (int i = 0; i < 3; i++) {
        token = strtok(NULL, " ");
        pos[i] = atof(token);
    }
    pos[3] = 1;

    for (int i = 0; i < 3; i++) {
        token = strtok(NULL, " ");
        dir[i] = atof(token);
    }

    glLightfv(lightConstant, GL_POSITION, pos);
    glLightfv(lightConstant, GL_SPOT_DIRECTION, dir);
    glLightf(lightConstant, GL_SPOT_CUTOFF, cutOffAngle);

    float dark[4] = {0.2, 0.2, 0.2, 1};
    float white[4] = {1, 1, 1, 1};
    glLightfv(lightConstant, GL_AMBIENT, white);
    glLightfv(lightConstant, GL_DIFFUSE, white);
    glLightfv(lightConstant, GL_SPECULAR, white);
    glLightfv(lightConstant, GL_EMISSION, white);
}

void Lighter ::lightPD(char *act, int w, int firstFrame) {

    char *buf = strdup(act);
    char *token = strtok(buf, " ");

    int lightConstant = getLightConstant(lightCounter++);

    if (lightConstant == ERROR && firstFrame) {
        cout << "ERROR: Maximum Number of Lights Exceeded(8). light  #"
             << lightCounter << " will be ignored"
             << "\n";
        return;
    }
    glEnable(lightConstant);

    float array[4];
    for (int i = 0; i < 3; i++) {
        token = strtok(NULL, " ");
        array[i] = atof(token);
    }
    array[3] = w;

    float dark[4] = {0, 0, 0, 1};
    float white[4] = {1, 1, 1, 1};
    glLightfv(lightConstant, GL_POSITION, array);
    glLightfv(lightConstant, GL_AMBIENT, white);
    glLightfv(lightConstant, GL_DIFFUSE, white);
    glLightfv(lightConstant, GL_SPECULAR, white);
    glLightfv(lightConstant, GL_EMISSION, white);
}
