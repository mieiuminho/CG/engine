#ifndef ENGINE_LIGHTER_H
#define ENGINE_LIGHTER_H
#include "../../lib/glew.h"
#include "../xmlParser.h"
#include <cstdlib>
#include <iostream>
#include <string.h>

class Lighter {

  private:
    int lightCounter = 0;

  public:
    Lighter() { this->lightCounter = 0; }

    void lightS(char *act, int firstFrame);

    void lightPD(char *act, int w, int firstFrame);

    void setLightCounter(int lightCounter);
};

#endif
