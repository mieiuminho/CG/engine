#ifndef ENGINE_MODEL_H
#define ENGINE_MODEL_H
#include <vector>

using namespace std;

class Model {

  private:
    vector<float> points;
    vector<float> normals;
    vector<float> textCoords;
    vector<unsigned int> indexes;

  public:
    Model() {}

    Model(vector<float> points, vector<float> normals, vector<float> textCoords,
          vector<unsigned int> indexes) {
        this->points = points;
        this->normals = normals;
        this->textCoords = textCoords;
        this->indexes = indexes;
    }

    vector<float> getPoints();

    vector<unsigned int> getIndexes();

    vector<float> getNormals();

    vector<float> getTextCoords();
};

#endif // ENGINE_MODEL_H
