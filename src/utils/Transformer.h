#ifndef ENGINE_TRANSFORMER_H
#define ENGINE_TRANSFORMER_H
#include "../splineCalculator.h"
#include <GL/glut.h>
#include <iostream>
#include <string.h>
#include <vector>

class Transformer {

  private:
  public:
    Transformer() = default;

    void translateDynamic(char *act, double t0);

    void translateStatic(char *act);

    void rotateDynamic(char *act, double t0);

    void rotateStatic(char *act);

    void scale(char *act);
};

#endif
