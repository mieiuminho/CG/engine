#ifndef GENERATOR_POINT_3D_H
#define GENERATOR_POINT_3D_H
#include <cstdio>
#include <cstdlib>

class Point_3D {

  private:
    float x;
    float y;
    float z;

  public:
    Point_3D() = default;

    Point_3D(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    Point_3D(Point_3D *p) {
        this->x = p->x;
        this->y = p->y;
        this->z = p->z;
    }

    float getX();

    float getY();

    float getZ();

    void setX(float x);

    void setY(float y);

    void setZ(float z);

    void setCoordenates(float x, float y, float z);

    char *toString();

    Point_3D crossProduct(Point_3D p);

    void normalize();
};

#endif
