#ifndef ENGINE_VBOUTILS_H
#define ENGINE_VBOUTILS_H
#include "../../lib/glew.h"
#include "Scene.h"
#include <cstdlib>

class VBOUtils {

  private:
    GLuint *buffers;
    GLuint *indexes;
    GLuint *normals;
    GLuint *textureCoordinates;

  public:
    VBOUtils() = default;

    GLuint *getBuffers();

    GLuint *getIndexes();

    GLuint *getNormals();

    GLuint *getTextureCoordinates();

    void allocate(Scene s);

    void generateBuffers(Scene s);
};

#endif
