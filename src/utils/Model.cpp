#include "Model.h"

vector<float> Model ::getPoints() { return this->points; }

vector<unsigned int> Model ::getIndexes() { return this->indexes; }

vector<float> Model ::getNormals() { return this->normals; }

vector<float> Model ::getTextCoords() { return this->textCoords; }
