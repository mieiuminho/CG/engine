#include "Transformer.h"

using namespace std;

static double *extractPoint(char *point) {
    char *save;
    double *r = (double *)malloc(sizeof(double) * 3);
    char *token = strtok_r(point, "|", &save);
    double x = atof(token);
    token = strtok_r(NULL, "|", &save);
    double y = atof(token);
    token = strtok_r(NULL, "|", &save);
    double z = atof(token);
    r[0] = x;
    r[1] = y;
    r[2] = z;
    return r;
}

void Transformer ::translateDynamic(char *act, double t0) {
    char *save;
    char *buff = strdup(act);
    char *token = strtok_r(buff, " ", &save);

    double x, y, z;
    double time = -1;
    double t1 = glutGet(GLUT_ELAPSED_TIME) / 1000.0;

    vector<double *> controlPoints;

    token = strtok_r(NULL, " ", &save);
    if (token) {
        time = atof(token);
    }
    token = strtok_r(NULL, " ", &save);
    while (token) {
        char *pointStr = strdup(token);
        controlPoints.push_back(extractPoint(pointStr));
        token = strtok_r(NULL, " ", &save);
    }

    if (controlPoints.size() < 4) {
        cout << "Error: The translate must have a minimum of 4 points";
        return;
    }

    double instant = t1 - t0;
    double *rotMat = (double *)malloc(sizeof(double) * 16);
    double *target = calculatePoint(controlPoints, rotMat, time, instant);
    x = target[0];
    y = target[1];
    z = target[2];
    glTranslatef(x, y, z);
    glMultMatrixd(rotMat);
}

void Transformer ::translateStatic(char *act) {
    char *token;
    char *buff = strdup(act);
    double x = 0, y = 0, z = 0;
    token = strtok(buff, " ");

    token = strtok(NULL, " ");

    if (token) {
        x = atof(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        y = atof(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        z = atof(token);
    }
    glTranslated(x, y, z);
}

void Transformer ::rotateDynamic(char *act, double t0) {
    char *buff = strdup(act);
    char *token = strtok(buff, " ");
    double time = 0;
    double t1 = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    int x = 0, y = 0, z = 0;

    token = strtok(NULL, " ");
    if (token) {
        time = atof(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        x = atoi(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        y = atoi(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        z = atoi(token);
    }
    double angle = 0;

    if (time > 0) {
        angle = (t1 - t0) * 360 / time;
        angle = (angle > 360 ? angle - 360 : angle);
    }

    glRotatef(angle, x, y, z);
}

void Transformer ::rotateStatic(char *act) {
    char *buff = strdup(act);
    char *token = strtok(buff, " ");
    int x = 0, y = 0, z = 0;
    double angle = 0;
    token = strtok(NULL, " ");
    if (token) {
        angle = atof(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        x = atoi(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        y = atoi(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        z = atoi(token);
    }
    glRotatef(angle, x, y, z);
}

void Transformer ::scale(char *act) {
    char *buff = strdup(act);
    char *token = strtok(buff, " ");
    double x = 0, y = 0, z = 0;

    token = strtok(NULL, " ");
    if (token) {
        x = atof(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        y = atof(token);
    }
    token = strtok(NULL, " ");
    if (token) {
        z = atof(token);
    }

    glScalef(x, y, z);
}