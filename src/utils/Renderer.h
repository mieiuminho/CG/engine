#ifndef ENGINE_RENDERER_H
#define ENGINE_RENDERER_H
#include "../../lib/glew.h"
#include "VBOUtils.h"
#include <GL/glut.h>
#include <iostream>
#include <string.h>
#include <vector>

using namespace std;

class Renderer {

  private:
    double t0;
    bool firstFrame;

  public:
    Renderer() {
        this->t0 = 0;
        this->firstFrame = true;
    }

    double getT0();

    bool isFirstFrame();

    void setT0(double t0);

    void setFirstFrame(bool firstFrame);

    void drawTextured(VBOUtils vbos, char *act, vector<unsigned int> textures,
                      vector<vector<unsigned int>> __indexes__);

    void drawMaterial(VBOUtils vbos, char *act,
                      vector<vector<unsigned int>> __indexes__);

    void bindToBuffers(VBOUtils vbos, Scene s);

    static void changeSize(int w, int h) {

        // Prevent a divide by zero, when window is too short
        // (you cant make a window with zero width).
        if (h == 0)
            h = 1;

        // compute window's aspect ratio
        float ratio = w * 1.0 / h;

        // Set the projection matrix as current
        glMatrixMode(GL_PROJECTION);

        // Load Identity Matrix
        glLoadIdentity();

        // Set the viewport to be the entire window
        glViewport(0, 0, w, h);

        // Set perspective
        gluPerspective(45.0f, ratio, 1.0f, 1000.0f);

        // return to the model view matrix mode
        glMatrixMode(GL_MODELVIEW);
    }

    static void idle() { glutPostRedisplay(); }
};

#endif