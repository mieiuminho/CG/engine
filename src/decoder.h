#ifndef PROJECT_DECODER_H
#define PROJECT_DECODER_H
#include "utils/Model.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string.h>
#include <vector>

using namespace std;

Model decode(const char *path);

#endif
