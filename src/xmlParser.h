#ifndef ENGINE_XMLPARSER_H
#define ENGINE_XMLPARSER_H
#define ST 901
#define DT 902
#define SR 903
#define DR 904
#define S 905
#define MM 906
#define MT 907
#define PO 908
#define PU 909
#define LP 910
#define LD 911
#define LS 912
#define ERROR -1
#include "../lib/glew.h"
#include "../lib/tinyxml2.h"
#include <string>
#include <vector>

int getActionVal(char *);
int getLightConstant(int);
std::vector<char *> parse(const char *, std::vector<const char *> *,
                          std::vector<const char *> *);

#endif // ENGINE_XMLPARSER_H
