#include "xmlParser.h"
#include <iostream>

using namespace tinyxml2;

int posFiles = 0;
int posTextures = 0;

int getActionVal(char *act) {
    if (act[0] == 'P') {
        if (strcmp(act, "PU") == 0)
            return PU;
        if (strcmp(act, "PO") == 0)
            return PO;
    } else if (act[1] == 'T') {
        if (strcmp(act, "ST") == 0)
            return ST;
        if (strcmp(act, "DT") == 0)
            return DT;
        if (strcmp(act, "MT") == 0)
            return MT;
    } else if (act[1] == 'R') {
        if (strcmp(act, "SR") == 0)
            return SR;
        if (strcmp(act, "DR") == 0)
            return DR;
    } else if (act[0] == 'L') {
        if (strcmp(act, "LP") == 0)
            return LP;
        if (strcmp(act, "LS") == 0)
            return LS;
        if (strcmp(act, "LD") == 0)
            return LD;

    } else {
        if (strcmp(act, "S ") == 0)
            return S;
        if (strcmp(act, "MM") == 0)
            return MM;
    }
    return ERROR;
}

int getLightConstant(int index) {
    switch (index) {
        case 0: {
            return GL_LIGHT0;
        }
        case 1: {
            return GL_LIGHT1;
        }
        case 2: {
            return GL_LIGHT2;
        }
        case 3: {
            return GL_LIGHT3;
        }
        case 4: {
            return GL_LIGHT4;
        }
        case 5: {
            return GL_LIGHT5;
        }
        case 6: {
            return GL_LIGHT6;
        }
        case 7: {
            return GL_LIGHT7;
        }
        default: { //
            return ERROR;
        }
    }
}

char *parseTrans(XMLElement *elem) {
    double x = 0, y = 0, z = 0;
    double time = -1;
    char *r;
    std::vector<char *> pointStrings;

    if (!elem->FindAttribute("time")) {
        r = (char *)malloc(sizeof(char) * 128);
        if (elem->FindAttribute("X")) {
            x = elem->FindAttribute("X")->DoubleValue();
        }
        if (elem->FindAttribute("Y")) {
            y = elem->FindAttribute("Y")->DoubleValue();
        }
        if (elem->FindAttribute("Z")) {
            z = elem->FindAttribute("Z")->DoubleValue();
        }
        sprintf(r, "ST %f %f %f", x, y, z);
        return r;
    }
    /**
     * Counts the number of point of the curve in order to allocate enough
     * memory It does so by going through all the 'point' children of the
     * element and keeping a counter
     * */
    int points = 0;
    XMLElement *pointCounter = elem->FirstChildElement("point");

    while (pointCounter) {
        points++;
        pointCounter = pointCounter->NextSiblingElement("point");
    }
    r = (char *)malloc(sizeof(char) * 32 * points + 10);

    time = elem->FindAttribute("time")->DoubleValue();
    XMLElement *point = elem->FirstChildElement("point");

    while (point) {
        x = 0;
        y = 0;
        z = 0;
        if (point->FindAttribute("X")) {
            x = point->FindAttribute("X")->DoubleValue();
        }
        if (point->FindAttribute("Y")) {
            y = point->FindAttribute("Y")->DoubleValue();
        }
        if (point->FindAttribute("Z")) {
            z = point->FindAttribute("Z")->DoubleValue();
        }
        sprintf(r, "%f|%f|%f", x, y, z);
        pointStrings.push_back(strdup(r));

        point = point->NextSiblingElement("point");
    }
    sprintf(r, "DT %f", time);

    for (int i = 0; i < pointStrings.size(); i++) {
        sprintf(r, "%s %s", r, pointStrings.at(i));
    }
    return r;
}

char *parseRotate(XMLElement *elem) {
    double time = 0;
    char *r = (char *)malloc(sizeof(char) * 64);
    int ax = 0, ay = 0, az = 0;

    if (elem->FindAttribute("angle")) {
        double angle = elem->FindAttribute("angle")->DoubleValue();

        if (elem->FindAttribute("axisX")) {
            ax = elem->FindAttribute("axisX")->IntValue();
        }
        if (elem->FindAttribute("axisY")) {
            ay = elem->FindAttribute("axisY")->IntValue();
        }
        if (elem->FindAttribute("axisZ")) {
            az = elem->FindAttribute("axisZ")->IntValue();
        }
        sprintf(r, "SR %f %d %d %d", angle, ax, ay, az);
        return r;
    }

    if (elem->FindAttribute("time")) {
        time = elem->FindAttribute("time")->DoubleValue();
    }
    if (elem->FindAttribute("axisX")) {
        ax = elem->FindAttribute("axisX")->IntValue();
    }
    if (elem->FindAttribute("axisY")) {
        ay = elem->FindAttribute("axisY")->IntValue();
    }
    if (elem->FindAttribute("axisZ")) {
        az = elem->FindAttribute("axisZ")->IntValue();
    }

    sprintf(r, "DR %f %d %d %d", time, ax, ay, az);

    return r;
}

char *parseScale(XMLElement *elem) {
    double x = 0, y = 0, z = 0;

    if (elem->FindAttribute("X")) {
        x = elem->FindAttribute("X")->DoubleValue();
    }
    if (elem->FindAttribute("Y")) {
        y = elem->FindAttribute("Y")->DoubleValue();
    }
    if (elem->FindAttribute("Z")) {
        z = elem->FindAttribute("Z")->DoubleValue();
    }

    char *r = (char *)malloc(sizeof(char) * 64);

    sprintf(r, "S %f %f %f", x, y, z);

    return r;
}

void parseColors(XMLElement *model, double *diffR, double *diffG, double *diffB,
                 double *specR, double *specG, double *specB, double *emissR,
                 double *emissG, double *emissB, double *ambR, double *ambG,
                 double *ambB) {

    model->QueryDoubleAttribute("diffR", diffR);
    model->QueryDoubleAttribute("diffG", diffG);
    model->QueryDoubleAttribute("diffB", diffB);

    model->QueryDoubleAttribute("specR", specR);
    model->QueryDoubleAttribute("specG", specG);
    model->QueryDoubleAttribute("specB", specB);

    model->QueryDoubleAttribute("emissR", emissR);
    model->QueryDoubleAttribute("emissG", emissG);
    model->QueryDoubleAttribute("emissB", emissB);

    model->QueryDoubleAttribute("ambR", ambR);
    model->QueryDoubleAttribute("ambG", ambG);
    model->QueryDoubleAttribute("ambB", ambB);
}

int duplicateFile(std::vector<const char *> *files, const char *fileName) {
    int pos = -1;
    const char **data = files->data();
    for (int i = 0; i < files->size() && pos == -1; i++) {
        if (strcmp(files->at(i), fileName) == 0) {
            pos = i;
        }
    }
    return pos;
}

void parseModels(XMLElement *elem, std::vector<const char *> *files,
                 std::vector<const char *> *textures,
                 std::vector<char *> *actions) {
    XMLElement *model = elem->FirstChildElement("model");
    while (model) {
        char *buf = nullptr;
        char *matStr = "";
        if (model->FindAttribute("texture")) {

            const char *textureToInsert =
                strdup(model->FindAttribute("texture")->Value());
            const char *fileToInsert =
                strdup(model->FindAttribute("file")->Value());

            textures->push_back(textureToInsert);
            int duplicate = duplicateFile(files, fileToInsert);

            if (duplicate == -1) {
                files->push_back(fileToInsert);
                asprintf(&buf, "MT %d %d", posFiles++, posTextures++);
            } else {
                asprintf(&buf, "MT %d %d", duplicate, posTextures++);
            }

            bool material = false;

            if (model->FindAttribute("material")) {
                material = model->FindAttribute("material")->BoolValue();
            }

            if (material) {
                double diffR = 0, diffG = 0, diffB = 0;
                double specR = 0, specG = 0, specB = 0;
                double emissR = 0, emissG = 0, emissB = 0;
                double ambR = 0, ambG = 0, ambB = 0;

                parseColors(model, &diffR, &diffG, &diffB, &specR, &specG,
                            &specB, &emissR, &emissG, &emissB, &ambR, &ambG,
                            &ambB);
                asprintf(&buf, "%s true %f %f %f %f %f %f %f %f %f %f %f %f",
                         buf, diffR, diffG, diffB, specR, specG, specB, emissR,
                         emissG, emissB, ambR, ambG, ambB);
            } else {
                asprintf(&buf, "%s false", buf);
            }

            actions->push_back(buf);

        } else {
            double diffR = 0, diffG = 0, diffB = 0;
            double specR = 0, specG = 0, specB = 0;
            double emissR = 0, emissG = 0, emissB = 0;
            double ambR = 0, ambG = 0, ambB = 0;

            parseColors(model, &diffR, &diffG, &diffB, &specR, &specG, &specB,
                        &emissR, &emissG, &emissB, &ambR, &ambG, &ambB);

            asprintf(&buf, "MM %d %f %f %f %f %f %f %f %f %f %f %f %f",
                     posFiles++, diffR, diffG, diffB, specR, specG, specB,
                     emissR, emissG, emissB, ambR, ambG, ambB);
            actions->push_back(buf);
            const char *toInsert =
                strdup(model->FindAttribute("file")->Value());
            files->push_back(toInsert);
        }

        model = model->NextSiblingElement("model");
    }
}

void parseLights(XMLElement *elem, std::vector<char *> *actions) {
    XMLElement *light = elem->FirstChildElement("light");

    while (light) {
        char *buf = nullptr;
        const char *type = strdup(light->FindAttribute("type")->Value());

        if (strcmp(type, "POINT") == 0) {
            double posX = 0, posY = 0, posZ = 0;

            light->QueryDoubleAttribute("posX", &posX);
            light->QueryDoubleAttribute("posY", &posY);
            light->QueryDoubleAttribute("posZ", &posZ);

            asprintf(&buf, "LP %f %f %f", posX, posY, posZ);
            actions->push_back(buf);
        } else if (strcmp(type, "DIRECTIONAL") == 0) {
            double dirX = 0, dirY = 0, dirZ = 0;

            light->QueryDoubleAttribute("dirX", &dirX);
            light->QueryDoubleAttribute("dirY", &dirY);
            light->QueryDoubleAttribute("dirZ", &dirZ);

            asprintf(&buf, "LD %f %f %f", dirX, dirY, dirZ);
            actions->push_back(buf);
        } else if (strcmp(type, "SPOT") == 0) {
            double dirX = 0, dirY = 0, dirZ = 0, posX = 0, posY = 0, posZ = 0,
                   angle = 0;

            light->QueryDoubleAttribute("dirX", &dirX);
            light->QueryDoubleAttribute("dirY", &dirY);
            light->QueryDoubleAttribute("dirZ", &dirZ);

            light->QueryDoubleAttribute("posX", &posX);
            light->QueryDoubleAttribute("posY", &posY);
            light->QueryDoubleAttribute("posZ", &posZ);

            light->QueryDoubleAttribute("angle", &angle);

            asprintf(&buf, "LS %f %f %f %f %f %f %f", angle, posX, posY, posZ,
                     dirX, dirY, dirZ);
            actions->push_back(buf);
        }

        light = light->NextSiblingElement("light");
    }
}

void parseGroup(XMLElement *group, std::vector<const char *> *files,
                std::vector<const char *> *textures,
                std::vector<char *> *actions) {
    XMLElement *elem = group->FirstChildElement();

    actions->push_back(strdup("PUSH"));

    while (elem) {

        const char *name = elem->Name();

        if (strcmp(name, "group") == 0) {
            parseGroup(elem, files, textures, actions);

        } else if (strcmp(name, "lights") == 0) {
            parseLights(elem, actions);

        } else if (strcmp(name, "translate") == 0) {
            actions->push_back(parseTrans(elem));

        } else if (strcmp(name, "rotate") == 0) {
            actions->push_back(parseRotate(elem));

        } else if (strcmp(name, "scale") == 0) {
            actions->push_back(parseScale(elem));

        } else if (strcmp(name, "models") == 0) {
            parseModels(elem, files, textures, actions);
        }
        elem = elem->NextSiblingElement();
    }

    actions->push_back(strdup("POP"));
}

std::vector<char *> parse(const char *filePath,
                          std::vector<const char *> *files,
                          std::vector<const char *> *textures) {
    std::vector<char *> actions;
    XMLDocument config;
    config.LoadFile(filePath);

    XMLElement *scene = config.FirstChildElement("scene");
    XMLElement *group = scene->FirstChildElement("group");
    XMLElement *lights = scene->FirstChildElement("lights");

    if (lights)
        parseLights(lights, &actions);

    while (group) {
        parseGroup(group, files, textures, &actions);
        group = group->NextSiblingElement("group");
    }
    return actions;
}
